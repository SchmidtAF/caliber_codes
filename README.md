# CALIBER MAPPER - functions to extract and map CALIBER data

This package allows users to download (`get`) disease codes from 
[https://www.caliberresearch.org/portal](https://www.caliberresearch.org/portal).

Once downloaded, `add` functions can be used to obtaining missing codes.
For example to map _read2_ codes to _read3_. 
The mapping function should work irrespective of the source they were
obtained from.

## Installation instructions

For an editable (developer) install run the 
command below from the root of the repository:

```
python -m pip install -e .
```

## Usage

The following functions are available:

#### `get_caliber`
```
# ~~~~~~~~~~~~~~~~~  read help 
get_caliber --help
    usage: get_caliber [-h] [--main [MAIN]] [--url [URL]] [--out_dir [OUT_DIR]]
    
    Scrape the caliber website
    
    optional arguments:
      -h, --help           show this help message and exit
      --main [MAIN]        Optional path to the parent directory the output
                           directory and files should be created.
      --url [URL]          url to a CALIBER table to scrape
      --out_dir [OUT_DIR]  The directory to store the output in. Default:
                           `original` is use the CALIBER table name

# ~~~~~~~~~~~~~~~~~ example 
get_caliber --url=https://www.caliberresearch.org/portal/show/phenotype_mi
    ### Extracting table: myo_infarct_gprd
    https://www.caliberresearch.org/portal/show/myo_infarct_gprd
    ### Extracting table: myo_infarct_hes
    https://www.caliberresearch.org/portal/show/myo_infarct_hes
    ### Skipping table: mi_minap
    ### Extracting table: lysis_opcs
    https://www.caliberresearch.org/portal/show/lysis_opcs
    ### Extracting table: mi_ons
    https://www.caliberresearch.org/portal/show/mi_ons
    ### Extracting table: mi_onsicd9
    https://www.caliberresearch.org/portal/show/mi_onsicd9
    
    ### Finished
```
Currently `get_caliber` only works for "starred" tables often denoted
with the phenotype prefix.
Will be expanded upon request. 
Similarly some types of tables might not be implemented yet (please 
create an issue).

### `add_codes`

The scraped CALIBER codes typically contain either read2, or ICD-10,
and exclude new/older version of the same codes based. 
The following CLI adds "missing" codes as an additional column to a file
containing a `codes` columns that needs to be mapped. 

Additionally it performs an optional _regex_ trim of the `codes` column.

For read codes mapping please consider including the `-match_tui` flag, 
to match on both read and tui columns.

```
# ~~~~~~~~~~~~~~~~~  read help 
add_codes --help
usage: add_codes [-h] [--file [FILE]] [--mapper [MAPPER]]
                 [--mapper_path [MAPPER_PATH]] [--regex [REGEX]]
                 [--main [MAIN]] [-match_tui] [-only_exact]

Convert code

optional arguments:
  -h, --help            show this help message and exit
  --file [FILE]         The file that needs to be mapped
  --mapper [MAPPER]     The desired target code
  --mapper_path [MAPPER_PATH]
                        The path to an optional mapper file. By default it
                        will use files pre-installed in the ./mappers
                        directory
  --regex [REGEX]       An optional regex expression to trim the source code
                        prior to mapping. Remember to surround the regex
                        expression with single quotes
  --main [MAIN]         Optional path to the parent directory, where the
                        output directory and files should be created.
  -match_tui            Read only -- if you want to match on tui as well as
                        read codes
  -only_exact           ICD only -- if you want exact matches only

# ~~~~~~~~~~~~~~~~~ example 
add_codes --file=af_gprd_read2.tsv --mapper=read3 --regex='[0-9a-zA-z]{2}$' -match_tui

#### Sourcing mapping file from: /home/amand/google_drive/Research/UCL/caliber_codes/mappers/RCTCTV3MAP.xmaps.txt.gz


#### FINISHED, the following file was created: af_gprd_read2_read3.tsv


```
