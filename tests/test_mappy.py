# testing mappy
import pandas as pd
import pytest
import os
from commandline_functions.mappy import (map_read, map_icd,
                                         _set_mapper_path, _trim,
                                         _extract
                                         )



# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# Adding data
here = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
# here='./'

pd.DataFrame({
    'codes': ['G301.00', 'G301.00', '14A4.00','14AH.00',
              # Adding AF codes; the added space is intentional
              '7L1H.12', '793Mz00', '7936A00',
              # Diabetes codes
              'C108212', 'C10EC11', 'C10F.00'
              ]
}).to_csv(os.path.join(here,'tests/test_data/read2.tsv'), sep='\t')

pd.DataFrame({
    'codes':[
    # TB codes
    '0010', '0019', '0120',
    # ANTHRAX codes
    '0220','0221','0222'
    ]
}).to_csv(os.path.join(here,'tests/test_data/icd9.tsv'), sep='\t', index=True)

pd.DataFrame({
    'codes':[
    # MI codes
    'I21', 'I22', 'I23',
    # AF codes
    'I48',
    # Diabets codes
    'E13', 'E14', ' G590'
    ]
}).to_csv(os.path.join(here,'tests/test_data/icd10.tsv'), sep='\t', index=True)

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# testing read3 mappings
def test_map_read3():
    map_to = 'read3'
    file = os.path.join(here,'tests/test_data/read2.tsv')
    mapper_path = _set_mapper_path(map_to)
    regex = '[0-9a-zA-z]{2}$'
    # running the function
    results=map_read(file=file, mapper=mapper_path, map_to=map_to, reg=regex)
    assert results.loc[results['codes']=='G301.00','read3'].values[0]=='G301.'
    assert results.loc[results['codes']=='14A4.00','read3'].values[0]=='14A4.'
    assert results.loc[results['codes']=='14AH.00','read3'].values[0]=='XaBL1'
    #assert results.loc[results['codes']=='7L1H.12','read3'].values[0]=='Xa1nl'
    assert results.loc[results['codes']=='793Mz00','read3'].values[0]=='XaMma'
    assert results.loc[results['codes']=='7936A00','read3'].values[0]=='7936A'
    #assert results.loc[results['codes']=='C108212','read3'].values[0]=='C1082'
    assert results.loc[results['codes']=='C10EC11','read3'].values[0]=='XaEno'
    assert results.loc[results['codes']=='C10F.00','read3'].values[0]=='X40J5'
    
    assert _trim(results, reg='[0-9a-zA-z]{2}$', col='codes')[0]=='G301.'
    assert _extract(results, reg='[0-9a-zA-z]{2}$', col='codes')[0]=='00'


# SAME TEST, with with tui
def test_map_read3_tui():
    map_to = 'read3'
    file = os.path.join(here,'tests/test_data/read2.tsv')
    mapper_path = _set_mapper_path(map_to)
    regex = '[0-9a-zA-z]{2}$'
    # running the function
    results=map_read(file=file, mapper=mapper_path, map_to=map_to, reg=regex,
                     include_tui=True)
    assert results.loc[results['codes']=='G301.00','read3'].values[0]=='G301.'
    assert results.loc[results['codes']=='14A4.00','read3'].values[0]=='14A4.'
    assert results.loc[results['codes']=='14AH.00','read3'].values[0]=='XaBL1'
    assert results.loc[results['codes']=='7L1H.12','read3'].values[0]=='Xa1nl'
    assert results.loc[results['codes']=='793Mz00','read3'].values[0]=='XaMma'
    assert results.loc[results['codes']=='7936A00','read3'].values[0]=='7936A'
    assert results.loc[results['codes']=='C108212','read3'].values[0]=='C1082'
    assert results.loc[results['codes']=='C10EC11','read3'].values[0]=='XaEno'
    assert results.loc[results['codes']=='C10F.00','read3'].values[0]=='X40J5'

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# testing ICD9 mappings
def test_map_icd9():
    map_to = 'icd9'
    file = os.path.join(here,'tests/test_data/icd10.tsv')
    mapper_path = _set_mapper_path(map_to)
    # running the function
    results=map_icd(file=file, mapper=mapper_path, map_to=map_to,
                    exact_match=False)
    # check results
    assert results.loc[results['codes']=='I21','icd9'].values.tolist() == ['413', 'UNDEF']
    assert results.loc[results['codes']=='I22','icd9'].values.tolist() == ['UNDEF']
    assert results.loc[results['codes']=='I23','icd9'].values.tolist() == ['UNDEF']
    assert results.loc[results['codes']=='I48','icd9'].values.tolist() == ['4273']
    assert results.loc[results['codes']=='E13','icd9'].values.tolist() == ['UNDEF']
    assert results.loc[results['codes']=='E14','icd9'].values.tolist() == ['UNDEF']
    assert results.loc[results['codes']==' G590','icd9'].values.tolist() == ['UNDEF']

def test_map_icd10():
    map_to = 'icd10'
    file = os.path.join(here,'tests/test_data/icd9.tsv')
    mapper_path = _set_mapper_path(map_to)
    # running the function
    results=map_icd(file=file, mapper=mapper_path, map_to=map_to)
    # check results
    assert results.loc[results['codes']=='0010','icd10'].values[0]=='A000'
    assert results.loc[results['codes']=='0019','icd10'].values[0]=='A009'
    assert results.loc[results['codes']=='0120','icd10'].values[0]=='UNDEF'
    assert results.loc[results['codes']=='0220','icd10'].values[0]=='A220'
    assert results.loc[results['codes']=='0221','icd10'].values[0]=='A221'
    assert results.loc[results['codes']=='0222','icd10'].values[0]=='A222'
