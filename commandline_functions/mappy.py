#!/usr/bin/env python
import argparse
import pandas as pd
import numpy as np
import re
import os
import csv
import gzip

def main():
    """
    The main entry point for the program
    """
    ###  getting arguments
    parser = set_args()
    args = parse_args(parser)
    
    ### check consitency
    if args.match_tui == True and (not args.mapper in ['read2','read3']):
        raise Warning('The `match_tui` flag is only supported for read targets')
    
    ###  Set directory
    if not args.main == 'current':
        os.chdir(args.main)
    
    ### Pick from default mappers
    if args.mapper_path == 'default':
        args.mapper_path = _set_mapper_path(args.mapper)
    
    ###  running the actual function
    if args.mapper in ['read2','read3']:
        results = map_read(args.file, args.mapper_path, args.mapper, args.regex,
                           include_tui=args.match_tui
                           )
        index_bool = False
    elif args.mapper in ['icd9', 'icd10']:
        results = map_icd(args.file, args.mapper_path, args.mapper, args.regex,
                          exact_match=args.only_exact
                           )
        index_bool = True
    else:
        raise ValueError('The program cannot yet map to the requested target')
    
    ### saving stuff
    file_split = os.path.splitext(args.file)
    out = file_split[0] + '_' + str(args.mapper) + '.tsv'
    results.to_csv(out, sep='\t', index=index_bool)
    
    ### printing
    print('\n\n#### FINISHED, the following file was created: ' +\
          os.path.basename(out) + '\n\n')


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# set-up command line interface
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def set_args(description='Convert code'):
    """
    Setup command line arguments but do not parse them
    
    Parameters
    ----------
    description : str, optional
    
    Returns
    -------
    parser : `argparse.ArgumentParser`
        A parser with all the arguments set up within it
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--file',
        type=str,
        help='The file that needs to be mapped',
        # default='./phenotype_af/af_gprd_read2.tsv',
        nargs='?'
        )
    parser.add_argument('--mapper',
        type=str,
        help='The desired target code',
        # default='read3',
        nargs='?'
        )
    parser.add_argument('--mapper_path',
        type=str,
        help='The path to an optional mapper file. By default it will use files\
        pre-installed in the ./mappers directory',
        nargs='?',
        default='default')
    parser.add_argument('--regex',
        type=str,
        help='An optional regex expression to trim the source code prior to \
            mapping. Remember to surround the regex expression with single \
            quotes',
        default=None,
        nargs='?'
        )
    parser.add_argument('--main',
        type=str,
        help='Optional path to the parent directory, where the\
        output directory and files should be created.',
        nargs='?',
        default='current'
        )
    parser.add_argument('-match_tui',
                        help='Read only -- if you want to match on tui as well\
                         as read codes',
                        action='store_true'
                        )
    parser.add_argument('-only_exact',
                        help='ICD only -- if you want exact matches only',
                        action='store_true'
                        )
    return parser

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# set-up command line interface

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_args(parser):
    """
    Parse the command line arguments
    
    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        A parser with all the arguments set up within it
    
    Returns
    -------
    args : :obj:`Namespace`
        An object with all the parsed command line arguments within it
    """
    args = parser.parse_args()
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# Getting default mapping files

def _set_mapper_path(target):
    """
    Set the default mapper paths
    """
    here = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
    if target == 'read2':
        # read3 to read2
        # mapper='mappers/CTV3RCTMAP.xmaps.txt.gz'
        mapper=os.path.join(here,'mappers/CTV3RCTMAP.xmaps.txt.gz')
    elif target == 'read3':
        # read2 to read3
        # mapper='mappers/RCTCTV3MAP.xmaps.txt.gz'
        mapper=os.path.join(here,'mappers/RCTCTV3MAP.xmaps.txt.gz')
    elif target in ['icd9','icd10']:
        # read2 to read3
        # mapper='mappers/RCTCTV3MAP.xmaps.txt.gz'
        mapper=os.path.join(here,'mappers/ukbb_map_icd9_to_icd10.tsv')
    else:
        raise IndexError('The specified target is not yet implimented')
    #print
    print('\n#### Sourcing mapping file from: ' + mapper)
    # return
    return mapper

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# The mapping functions

def _extract(df, reg, col='codes'):
    """
    Regex extract matching part of string
    """
    l = [re.search(reg, str(s)).group(0) for s in df[col].str.strip()]
    return l

def _trim(df, reg, col='codes'):
    """
    Regex trimming of specific column
    """
    l = [re.sub(reg, '', str(s)) for s in df[col].str.strip()]
    return l

def map_read(file, mapper, map_to='read3', reg=None, include_tui=False,
             s_col=0, t_col=2, s_tui=1, t_tui=3, sep='\t'):
    """
    Will map read2 to read3 and return a curated file with two additional
    columns named; `read2` and `read3`.
    
    Arguments
    ---------
    file:           :string: the path to the file that needs to be mapped
    mapper:         :string: the path the the mapper file
    map_to:         :string: the mapping direction; default is to read3
    reg:            :string: regex expression to optional trim the codes column
    [st]_col        :int: the position of the source and target columns
    [st]_tui        :int: the position of the source and target tui columns
    sep:            :string: the column delimiter
    include_tui:    :boolean: if true match on read AND tui
    """
    # getting the column names
    if map_to == 'read3':
        new_c = 'read3'; old_c='read2'
        new_t = 'read3_tui'; old_t = 'read2_tui'
    elif map_to == 'read2':
        new_c = 'read2'; old_c='read3'
        new_t = 'read2_tui'; old_t = 'read3_tui'
    else:
        raise IndexError('map_to should be read2 or read3')
    
    # removing any part of the codes
    original = pd.read_csv(file, sep=sep, dtype='str')
    original.dropna(axis=0, how='all', inplace=True)
    if reg is not None:
        # private functions internally set to string
        original[old_c] = _trim(original, reg)
        original[old_t] = _extract(original,reg)
    else:
        # simply select last 2chr, and remove last 2chr
        # do some trimming just to be sure
        original['codes'] = original.codes.astype(str).str.strip().copy()
        original[old_c] = original.codes.str[:-2]
        original[old_t] = original.codes.str[-2:]
    # mapping the codes
    keys = []
    new_codes = []
    new_tui = []
    # do we need to include tui
    # NOTE this really should be moved to a function
    if include_tui == True:
        for r in original.iterrows():
            if re.search('gz$', mapper):
                # using gzip
                with gzip.open(mapper, 'rt') as infile:
                    contenter=csv.reader(infile, delimiter=sep)
                    for row in contenter:
                        # remember S is the source and T the target
                        if row[s_col] == r[1][old_c] and row[s_tui] == r[1][old_t]:
                            keys.append(r[1][old_c])
                            new_codes.append(row[t_col])
                            new_tui.append(row[t_tui])
            else:
                # normal
                with open(mapper, 'rt') as infile:
                    contenter=csv.reader(infile, delimiter=sep)
                    for row in contenter:
                        # remember S is the source and T the target
                        if row[s_col] == r[1][old_c] and row[s_tui] == r[1][old_t]:
                            keys.append(r[1][old_c])
                            new_codes.append(row[t_col])
                            new_tui.append(row[t_tui])
    # else only select on read code
    else:
        for k in original[old_c]:
            if re.search('gz$', mapper):
                with gzip.open(mapper, 'rt') as infile:
                    contenter=csv.reader(infile, delimiter=sep)
                    for row in contenter:
                        # remember S is the source and T the target
                        if row[s_col]==k:
                            keys.append(k)
                            new_codes.append(row[t_col])
                            new_tui.append(row[t_tui])
            else:
                with open(mapper, 'rt') as infile:
                    contenter=csv.reader(infile, delimiter=sep)
                    for row in contenter:
                        # remember S is the source and T the target
                        if row[s_col]==k:
                            keys.append(k)
                            new_codes.append(row[t_col])
                            new_tui.append(row[t_tui])
    
    # CHECK FOR INTERNAL CONSISTENCY
    if len(keys) != len(new_codes):
        raise IndexError('The are different numbers of new and old codes')
    
    # ~~~~~~~~~~~~~ Add to dataframe
    # First creating a update pd.DataFrame
    # This will ensure necessary row repeasts when merging
    update = pd.DataFrame({new_c: new_codes, new_t: new_tui}, index = keys)
    # removing duplicates
    update = update.loc[~update.duplicated()].copy()
    # merging on left keys (the only type of keys)
    new = pd.merge(original,update,how='left', left_on=old_c, right_index=True)
    # new = new.loc[~new.duplicated()].copy()
    # return stuff
    return new

def map_icd(file, mapper, map_to='icd10', reg=None, exact_match=True,
            s_col=None, t_col=None, sep='\t'):
    """
    Will map ICD codes from 9 to 10 and back.  Returns a curated file with two
    additional columns named: `icd9` and `icd10`.
    
    Arguments
    ---------
    file:           :string: the path to the file that needs to be mapped
    mapper:         :string: the path the the mapper file
    map_to:         :string: the mapping direction; default is to icd10
    reg:            :string: regex expression to optional trim the codes column
    exact_match:    :boolean: if codes should expaned to match possible
                        sub-clasifications. Default: True.
                        For map_to=icd9 the regex is:
                        k_ex = '{0}[0-9]{{{1},{2}}}(X|A|D|S)*'.format(k,0, m)
                        for map_to=icd10:
                        k_ex = '{0}[0-9]{{{1},{2}}}'.format(k,0, m)
    [st]_col        :int: the position of the source and target columns
                    default = None, to use defaults dependent on map_to setting
    sep:            :string: the column delimiter
    """
    # getting the column names
    if map_to == 'icd9':
        new_c = 'icd9'; old_c='icd10'
        if s_col is None:
            s_col = 2
        if t_col is None:
            t_col = 0
    elif map_to == 'icd10':
        new_c = 'icd10'; old_c='icd9'
        if s_col is None:
            s_col = 0
        if t_col is None:
            t_col = 2
    else:
        raise IndexError('map_to should be either icd10 or icd9')
    
    # removing any part of the codes
    original = pd.read_csv(file, sep=sep, index_col=0, dtype='str')
    original.dropna(axis=0, how='all', inplace=True)
    if reg is not None:
        # private function, internally sets to string
        original[old_c] = _trim(original, reg)
    else:
        # simply rename and strip leading/trailing white spaces
        original[old_c] = original.codes.astype(str).str.strip().copy()
    
    # mapping the codes
    keys = []
    new_codes = []
    # do we need to include tui
    # note this really should be moved to a function
    for k in original[old_c]:
        # if k is less than 4 characters, add possible trailing characters
        # also include possible extentions
        if exact_match == False:
            if map_to == 'icd9':
                m = np.max([4 - len(k), 0])
                k_ex = '{0}[0-9]{{{1},{2}}}(X|A|D|S)*'.format(k,0, m)
            else:
                m = np.max([4 - len(k), 0])
                k_ex = '{0}[0-9]{{{1},{2}}}'.format(k,0, m)
        else:
            k_ex = k
        # should also map to codes ending on x
        with open(mapper, 'rt') as infile:
            contenter=csv.reader(infile, delimiter=sep)
            for row in contenter:
                # remember s is the source and t the target
                # also mapping on 7th character: x placeholder, a initial,
                #   d subsequent, s sequela.
                if bool(re.fullmatch(k_ex, row[s_col])):
                    # print(k +' : ' + row[s_col])
                    keys.append(k)
                    new_codes.append(row[t_col])
    
    
    # CHECK FOR INTERNAL CONSISTENCY
    if len(keys) != len(new_codes):
        raise IndexError('The are different numbers of new and old codes')
    
    # ~~~~~~~~~~~~~ Add to dataframe
    # First creating a update pd.DataFrame
    # This will ensure necessary row repeasts when merging
    update = pd.DataFrame({new_c: new_codes}, index = keys)
    # removing duplicates also consider index
    update['index'] = update.index
    update = update.loc[~update.duplicated()].copy()
    del update['index']
    # merging on left keys (the only type of keys)
    new = pd.merge(original,update,how='left', left_on=old_c, right_index=True)
    # new = new.loc[~new.duplicated()].copy()
    # return stuff
    return new

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()

