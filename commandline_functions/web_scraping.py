#!/usr/bin/env python
import requests
import bs4
import re
import os
import pandas as pd
import argparse

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# The main function that will run everything

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """
    The main entry point for the program
    """
    
    ###  getting arguments
    parser = set_args()
    args = parse_args(parser)
    
    ###  Set directory
    if not args.main == 'current':
        os.chdir(args.main)
    
    ###  running the actual function
    scrapy(phenotype=args.url, directory=args.out_dir)
    ### END

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# set-up command line interface

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def set_args(description='Scrape the caliber website'):
    """
    Setup command line arguments but do not parse them
    
    Parameters
    ----------
    description : str, optional
    
    Returns
    -------
    parser : `argparse.ArgumentParser`
        A parser with all the arguments set up within it
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--main',
                        type=str,
                        help='Optional path to the parent directory, where the\
                        output directory and files should be created.',
                        nargs='?',
                        default='current'
                        )
    parser.add_argument('--url',
                        type=str,
                        help='url to a CALIBER table to scrape',
                        nargs='?',
                        # default='https://www.caliberresearch.org/portal/show/phenotype_diabetes'
                        )
    parser.add_argument('--out_dir',
                        type=str,
                        help='The directory to store the output in. Default:\
                        `original` will use the original CALIBER table name',
                        nargs='?',
                        default='original'
                        )
    return parser

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_args(parser):
    """
    Parse the command line arguments
    
    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        A parser with all the arguments set up within it
    
    Returns
    -------
    args : :obj:`Namespace`
        An object with all the parsed command line arguments within it
    """
    args = parser.parse_args()
    return args

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# Actual scraping function

# phenotype='https://www.caliberresearch.org/portal/show/phenotype_af'
# phenotype='https://www.caliberresearch.org/portal/show/phenotype_mi'
# phenotype='https://www.caliberresearch.org/portal/show/phenotype_hf'

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _bs_text_to_df(input):
    '''
    Turns text objects with '\t' and '\n' to pd.DF
    '''
    table=[]
    # if len(input) == 1:
    #     cont = input.contents[0]
    #     table.append([t.split('\t') for t in cont.split('\n')])
    #     res = pd.DataFrame(table[0])
    # else:
    for content in input:
        cont = content.contents[0]
        table.append([t.split('\t') for t in cont.split('\n')])
    if table[0][0] == ['']:
        res = pd.DataFrame(table[0][1:])
    else:
        res = pd.DataFrame(table[0])
    return res

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _set_names(input, direct):
    '''
    Set column names and output
    '''
    colnames = ['Category', 'Category_code', 'codes', 'term', 'misc']
    # NOTE (?!\w) indicates end of word
    if re.match('.*(_gprd|primarycare)(?!\w)', input):
        output = os.path.join(direct, input + '_read2.tsv')
    elif re.match('^.*(_hes|secondarycare|death)(?!\w)', input):
        output = os.path.join(direct, input + '_icd10.tsv')
    elif re.match('^.*_ons(?!\w)', input):
        output = os.path.join(direct, input + '_icd10.tsv')
    elif re.match('^.*_onsicd9(?!\w)', input):
        output = os.path.join(direct, input + '_icd9.tsv')
    elif re.match('^.*_opcs(?!\w)', input):
        output = os.path.join(direct, input + '_opcs4.tsv')
        # ignore any other table
    else:
        # print('### Skipping table: ' + code)
        colnames='continue'
        output='continue'
    if re.match('^.*(primarycare|secondarycare|death)(?!\w)', input):
        colnames = colnames[2:4]
    return colnames, output

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _fetch_categories_table(table, input, direct):
    """
    Private function to extract the factor levels
    """
    table_rows = table.find_all('tr')
    # Traverse over all the rows and extract rows that presents
    # Category & Definition information
    categories_def_rows = []
    categories_def_colname = ""
    is_categories_def = False
    
    for row in table_rows:
        row_td_text = [td.text.strip() for td in row.find_all("td")]
        if len(row_td_text) == 2 and "Category" in row_td_text and "Definition" in row_td_text:
            categories_def_colname = row_td_text
            is_categories_def = True
        elif is_categories_def and len(row_td_text) == 2 and re.match('^[0-9]+$', row_td_text[0]):
            categories_def_rows.append(row_td_text)
        else:
            is_categories_def = False
    
    output_filepath = os.path.join(direct, input + '_categories_definition.tsv')
    
    if len(categories_def_rows) > 0:
        df = pd.DataFrame(categories_def_rows, columns = categories_def_colname)
        # print(df.head())
        df.to_csv(output_filepath, sep = "\t", index = False)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _write_flatfile(string, output):
    '''
    Writes string to output
    '''
    if not string.isspace():
        text_file = open(output, "w")
        n = text_file.write(string)
        text_file.close()

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _table_with_tab(soup,direct):
    '''
    Extract data from a table with tabs
    '''
    for head in ['primarycare','secondarycare','death']:
        codes_list = soup.find_all('div', attrs={'id':head})
        table = _bs_text_to_df(codes_list[0].find_all('pre'))
        cnam, output = _set_names(codes_list[0].attrs['id'],direct)
        table.columns = cnam
        table.to_csv(output,sep='\t',index=False)
    #### GETTING THE IMPLIMENTATION FILE
    try:
        implementation = soup.find_all('div', attrs={'id':'implementation'})
        implement = []
        for imp in implementation:
            for im in imp.find_all('p'):
                # print(im)
                implement = implement + [im.contents[0]] + ['\n\n']
        _write_flatfile(implement[0],os.path.join(direct, 'IMPLEMENTATION.txt'))
    except:
        print('Did not find an implementation file')
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _extract_category_code(category_value):
    # Extract category code from table "Coding lists".
    pattern_category = re.compile("\([0-9]+\)$")
    groups = pattern_category.search(category_value)
    if groups:
        group = groups.group(0)
        category_name = category_value.replace(group, "")
        return category_name, group[1:-1]
    return category_value.text.strip(), ""
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _table_normal(tables, direct, url_domain):
    '''
    Extract data from a regular table
    '''
    #### CREATE README files
    # TODO: prefer to not use numeric index to select tables
    text = tables[0].get_text()
    _write_flatfile(text,os.path.join(direct,"README.txt"))
    #### EXTRACT IMPLEMENTATION PART
    try:
        implement = re.split('Implementation', text, flags=re.IGNORECASE)[1]
        _write_flatfile(implement,os.path.join(direct,"IMPLEMENTATION.txt"))
    except IndexError:
        pass
    
    #### GETTING THE CODES
    rows = []
    for s in tables:
        rows = rows + s.find_all('td')
    for row in rows:
        link = row.find('a')
        # getting subphenotype codes
        if link:
            # type of table
            subtab = link.get('href')
            code = re.split('/', subtab)[-1]
            # NOTE: Need to strip the last 2 (TUI) characters from the read codes
            # Should do this in a different script to first get the clean
            # CALIBER downloads
            colnames, output = _set_names(code, direct)
            if colnames == 'continue':
                print('### Skipping table: ' + code)
                continue
            # accessing table
            print('### Extracting table: ' + code)
            subtype = str(url_domain) + link.get('href')
            print(subtype)
            soupling = bs4.BeautifulSoup(requests.get(subtype).text, features="html.parser")
            all_tables = soupling.find_all('table', attrs = {'class' : 'table table-condensed'})
            # getting the factor levels
            _fetch_categories_table(all_tables[0], code, direct)
            
            # Extract table containing the codes.
            tabling = all_tables[-1]
            table_rows = tabling.find_all('tr')
            lines = []
            for tr in table_rows:
                td = tr.find_all('td')
                row = [tr.text.strip() for tr in td]
                if len(row) > 0:
                    if 'Category_code' in colnames:
                        # Extract the category code from a column
                        category_name, category_code = _extract_category_code(row[0])
                        row[0] = category_name
                        row.insert(1, category_code)
                    if row[-1] == "":
                        row = row[:-1]
                        if len(colnames) > len(row):
                            # Remove column with empty values.
                            colnames = colnames[:-1]
                    lines.append(row)

            df = pd.DataFrame(lines, columns = colnames)
            df.to_csv(output, sep = "\t", index = False)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def scrapy(phenotype, directory):
    '''
    Scrapes the CALIBER website and creates a phenotype specific directory \
    to various tables with codes as well as a README files and \
    optional IMPLEMENTATION files.
    
    Will check if there are any optional sub-phenotype tables and extract\
    these.
    
    Arguments
    ----------
    phenotype : :sring
        The url to the phenotype table(s).
    directory : :string
        The directory name to store the output in.
    Returns
    ----------
    Writes verbatim tables as tsv to the directory and tries to include README
    and IMPLEMENTATION text files
    
    Echo's the tables found and if these were extracted or skipped. Has some
    provisions for tables organized in tabs.
    '''
    
    #### get domain
    url_split = re.split('/', phenotype)
    url_domain = "{}//{}".format(url_split[0], url_split[2])
    #### create directory
    direct = url_split[-1]
    if not os.path.exists(direct):
        os.makedirs(direct)
    #### accessing the website
    response = requests.get(phenotype)
    soup = bs4.BeautifulSoup(response.text, features="html.parser")
    connect={}
    idx = []
    tables = soup.find_all('table', attrs={'class':'table table-condensed'})
    #### IF THE WEBSITE CONTIANS TABS DO THIS
    if len(tables) == 1:
        # taking only a subset of tabs
        _table_with_tab(soup, direct)
        print('\n### Warning table with tabs found - please check output: ' + str(direct) )
    #### ELSE GO ON WITH A NORMAL TABLE
    else:
        _table_normal(tables, direct, url_domain)
    # END
    print('\n\n### Finished\n\n')

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
